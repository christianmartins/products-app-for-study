class product{
    constructor(
        id,
        description,
        stock,
        price,
        imageUrl
    ){
        this.id = id;
        this.description = description;
        this.stock = stock;
        this.price = price;
        this.imageUrl = imageUrl;
    }
}
import { http } from './config'

export default {

    fetch:() => {
        return http.get('findAll')
    },

    save:(product) => {
        return http.post("", product)
    },

    update:(product) => {
        return http.put("", product)
    },

    delete:(product) =>{
        return http.delete("", product)
    }

}
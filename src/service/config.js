import axios from 'axios'

export const http = axios.create({
    baseURL: 'https://product-list-for-study.herokuapp.com/product/'
})